# README #

This README would normally document whatever steps are necessary to get your application up and running.

Basic dice game... Compete against the computer, first to the target number WINS!

# Rules: #

Roll the die! If you roll anything other than a 1, the score is added to your total score for that round.

- Want another roll? Go for it! As long as you don't get a 1, then you can roll as many concecutive times as you like.

- If you decide not to take another roll then your score for that turn is totalled up and added to your overall score for the game.

BUT... If you roll a 1 then your turn is over, and your score for that round is 0.

Now it's the computers turn!

Keep going until one of you hits the target score (e.g 100)
