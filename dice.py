import random
import time
from fonts import Color
comp_total = [0]
human_total = [0]
# random.seed(1)



# Colours class
def get_color(num):

    colours = [Color.YELLOW + Color.BOLD + u'\u2680' + Color.END,

               Color.GREEN + Color.BOLD + u'\u2681' + Color.END,

               Color.BLUE + Color.BOLD + u'\u2682' + Color.END,

               Color.CYAN + Color.BOLD + u'\u2683' + Color.END,

               Color.RED + Color.BOLD + u'\u2684' + Color.END,

               Color.DARKCYAN + Color.BOLD + u'\u2685' + Color.END]

    return colours[num-1]



def comp_move(current, win_target):

    num_of_turns = random.randint(1, 9)

    score = 0


    i = 0

    while i < num_of_turns:

        roll = random.randint(1, 6)

        if roll == 1:

            # <editor-fold desc="Print to console">

            print(Color.RED + '--------------------------------------------' + Color.END)

            print(Color.BOLD + "Computer rolled a 1 - ha ha" + Color.END)

            print(Color.RED + '--------------------------------------------' + Color.END)

            # </editor-fold>

            who_next('human', 0, win_target)

        else:

            score += roll

            if current + score >= int(win_target):

                # <editor-fold desc="Print to console">

                print("Computer's roll " + str(i + 1) + ": " + get_color(roll) + " - " + str(roll))

                print('--------------------------------------------')

                print(Color.RED + 'Computer wins' + Color.END)

                print('--------------------------------------------')

                # </editor-fold>

                quit()

            else:

                print("Computer's roll " + str(i + 1) + ": " + get_color(roll) + " - " + str(roll))

        time.sleep(1)

        i = i + 1

    who_next('human', score, win_target)



def stay_go_question():

    yes_no = input('Would you like to roll? Y or N: ')

    return yes_no



def human_move(current, tmp, win_target):

    score = tmp

    stay_go = stay_go_question()

    if stay_go.lower() == 'y':

        roll = random.randint(1, 6)

        if roll == 1:

            # <editor-fold desc="Print to console">

            print(Color.RED + '--------------------------------------------' + Color.END)

            print(Color.BOLD + "Bad luck, you got 1" + Color.END)

            print(Color.RED + '--------------------------------------------' + Color.END)

            # </editor-fold>

            who_next('comp', 0, win_target)

        else:

            score += roll

            if current + score >= int(win_target):

                # <editor-fold desc="Print to console">

                print("You got: " + get_color(roll) + " - " + str(roll))

                print('--------------------------------------------')

                print(Color.GREEN + 'Human wins' + Color.END)

                print('--------------------------------------------')

                # </editor-fold>

                quit()

            else:

                print("You got: " + get_color(roll) + " - " + str(roll))

        human_move(current, score, win_target)

    else:

        who_next('comp', score, win_target)



def who_next(who, a_score, win_target):

    global comp_total, human_total

    if who == 'comp':

        human_total.append(a_score)

        # <editor-fold desc="Print to console">

        print("Computer Total: " + str(sum(comp_total)))

        print("Human Total: " + str(sum(human_total)))

        print("\nCOMPUTERS TURN")

        # </editor-fold>

        time.sleep(2)

        comp_move(sum(comp_total), win_target)

    else:

        comp_total.append(a_score)

        # <editor-fold desc="Print to console">

        print("Computer Total: " + str(sum(comp_total)))

        print("Human Total: " + str(sum(human_total)))

        print("\nHUMANS TURN")

        # </editor-fold>

        human_move(sum(human_total), 0, win_target)



def init():

    win_target = input("What shall we play up to? ")

    who_first = input("Who's going first? comp or human? ")

    who_next(who_first, 0, win_target)



if __name__ == "__main__":

    init()
