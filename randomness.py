import random
import dice
from fonts import Color


numbers = {1: 0, 2: 0, 3: 0, 4: 0, 5: 0, 6: 0}

for i in range(100000):
	roll = random.randint(1, 6)
	numbers[roll] += 1


print("from 100,000 rolls:")
print(Color.YELLOW + Color.BOLD + u'\u2680' + Color.END + " - " + str(numbers[1]))
print(Color.RED + Color.BOLD + u'\u2681' + Color.END + " - " + str(numbers[2]))
print(Color.BLUE + Color.BOLD + u'\u2682' + Color.END + " - " + str(numbers[3]))
print(Color.GREEN + Color.BOLD + u'\u2683' + Color.END + " - " + str(numbers[4]))
print(Color.CYAN + Color.BOLD + u'\u2684' + Color.END + " - " + str(numbers[5]))
print(Color.DARKCYAN + Color.BOLD + u'\u2685' + Color.END + " - " + str(numbers[6]))


dice.init()
